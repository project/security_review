<?php

namespace Drupal\Tests\security_review\Functional\Plugin;

use Drupal\Tests\security_review\Functional\SecurityReviewTestBase;
use Drupal\user\Entity\Role;
use Drupal\user\RoleInterface;

/**
 * Contains tests for Check that don't suffice with KernelTestBase.
 *
 * @group security_review
 */
class AdminPermissionTest extends SecurityReviewTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['node'];

  /**
   * Tests AdminPermission plugin.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testAdminPermission(): void {
    // Run tests.
    $this->runTestsFromUi();

    // Verify test passed.
    $this->assertSession()->pageTextContainsOnce('Untrusted roles do not have administrative or trusted Drupal permissions.');

    // Grant an admin permission to an untrusted role.
    $role = Role::load(RoleInterface::AUTHENTICATED_ID);
    $role->grantPermission('administer nodes')->save();

    // Run tests again.
    $this->runTestsFromUi();

    // Verify test failed.
    $this->assertSession()->pageTextContainsOnce('Untrusted roles have been granted administrative or trusted Drupal permissions.');

    // Now verify the permission.
    $this->drupalGet('/admin/reports/security-review/help/security_review/administrative_permissions');
    $this->assertSession()->pageTextContainsOnce('Authenticated user has the following restricted permission');
    $this->assertSession()->pageTextContainsOnce('administer nodes');
  }

}
