<?php

namespace Drupal\Tests\security_review\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\user\UserInterface;

/**
 * Base class for testing Security review functionality.
 */
abstract class SecurityReviewTestBase extends BrowserTestBase {

  /**
   * An admin user with administrative permissions for security review.
   *
   * @var \Drupal\user\UserInterface
   */
  protected UserInterface $adminUser;

  /**
   * The permissions required for an admin user to run security review.
   *
   * @var array
   *   A list of permissions.
   */
  protected array $permissions = ['access security review list', 'run security checks'];

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['security_review'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create an admin user.
    $this->adminUser = $this->drupalCreateUser($this->permissions);
    $this->drupalLogin($this->adminUser);
  }

  /**
   * Simple function to run the entire checklist from the UI.
   */
  protected function runTestsFromUi(): void {
    $this->drupalGet('/admin/reports/security-review');
    $this->assertSession()->buttonExists('Run checklist');
    $this->submitForm([], 'Run checklist');
  }

}
